CORDOVA 

# What is Cordova ?#

Cordova ,formerly called as Phone Gap is a platform to build Native Mobile Applicatons using HTML5, CSS and Java Script.

In other words it acts  a container for running a web application written in HTML, CSS,JS Typically Web applications cannot use the native device functionality like Camera, GPS, Accelerometer , Contacts etc.

# INSTALL #
npm install -g cordova

cordova -v

# ǸEW PROJECT#
* cordova create HelloWorld
* cd HelloWorld
* cordova platform add android
* cordova build
* cordova emulate android 
* cordova run android

# PLUGIN #
* cordova plugin add cordova-plugin-contacts
* cordova plugin remove cordova-plugin-contacts	


#RUN EMULATOR SERVICE#

adb devices (list of devices)

adb kill-server (kill server)

adb start-server (start server)

#PATH PLATFORM-TOOLS#
path: C:\Users\[USER]\AppData\Local\Android\sdk\platform-tools


# DOCUMENTATION #
https://cordova.apache.org/docs/en/latest/guide/cli/index.html

https://www.tutorialspoint.com/cordova/